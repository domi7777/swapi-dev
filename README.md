[![pipeline status](https://gitlab.com/domi7777/swapi-dev/badges/master/pipeline.svg)](https://gitlab.com/domi7777/swapi-dev/-/commits/master)
[![coverage report](https://gitlab.com/domi7777/swapi-dev/badges/master/coverage.svg)](https://gitlab.com/domi7777/swapi-dev/-/commits/master)

# A NodeJS Exercise: Star Wars Planets Diameter Calculator

Calculates the total diameter of the planets **with mountains and with a water surface** for a specific film.

Forked from [https://gitlab.qualif.io/interview/swapi-dev](https://gitlab.qualif.io/interview/swapi-dev)

See the exercise [instructions](doc/instructions.md)

## How to run:
There are two ways of starting this application:
- [1. Command line usage](#cmd_line)
- [2. Web application](#web_app)

<a name="cmd_line"></a>
## 1. Command line Usage:

```bash
$ npm start 6
```

or

```bash
$ npm run compile
$ node dist/index.js 6
```

### Output:
```bash
> In Film #6 "Revenge of the Sith" there are 2 planets that have mountains and a water surface (> 0):
> - Alderaan, diameter: 12500
> - Naboo, diameter: 12120
> Total diameter: 24620
```

<a name="web_app"></a>
## 2. Web app

### 2.1 Run web app:
```bash
$ npm run compile
$ npm run start:web-api
```
or
```bash
$ npm run compile
$ node dist/web-application.js
```
and go to [http://localhost:8080](http://localhost:8080)

---

### 2.2 Run web app with auto-reload of Angular:
```bash
$ npm run start:web-api
```
and in another console:
```bash
$ npm run ng:serve
```
and navigate to [http://localhost:4200](http://localhost:4200)
