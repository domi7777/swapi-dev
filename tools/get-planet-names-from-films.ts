import 'reflect-metadata';
import { Container } from 'typedi';
import { FilmService } from '../src/api/films/film.service';

(async () => {
    const filmService = Container.get(FilmService);
    const planets = new Set<string>();
    for (let i = 1; i <= 6; i++) {
        const film = await filmService.getFilmWithPlanets(i);
        film.planets.forEach((planet) => planets.add(planet.name));
    }
    console.log(Array.from(planets).sort());
})();
