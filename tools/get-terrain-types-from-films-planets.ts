import 'reflect-metadata';
import { Container } from 'typedi';
import { FilmService } from '../src/api/films/film.service';

/**
 * To build TerrainType enum
 */
(async () => {
    const filmService = Container.get(FilmService);
    const terrains = new Set<string>();
    for (let i = 1; i <= 6; i++) {
        const film = await filmService.getFilmWithPlanets(i);
        film.planets.forEach((planet) => planet.terrains.forEach((terrain) => terrains.add(terrain)));
    }
    console.log(Array.from(terrains).sort());
})();
