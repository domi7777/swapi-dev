export interface Transformer<T, R> {
    transform(response: T, ...params: unknown[]): R;
}
