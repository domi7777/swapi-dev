// see https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color
export const ConsoleColor = {
    cyan: '\x1b[36m%s\x1b[0m',
    red: '\x1b[31m%s\x1b[0m',
};
