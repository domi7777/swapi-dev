import { PlanetResponseTransformer } from './planet-response.transformer';
import { Planet, PlanetResponse, TerrainType } from '../../shared/planets/planet.model';

describe('PlanetResponseTransformer', () => {
    let transformer: PlanetResponseTransformer;

    beforeEach(() => {
        transformer = new PlanetResponseTransformer();
    });

    it('should transform terrains', () => {
        // Given
        const rawTerrains = 'grasslands, mountains';

        // When
        const terrains: TerrainType[] = transformer['getTerrains'](rawTerrains);

        // Then
        expect(terrains).toEqual([TerrainType.grasslands, TerrainType.mountains]);
    });

    it('should get water percentage', () => {
        // Given
        const rawSurfaceWater = '0.5';

        // When
        const waterPercentage: number = transformer['getWaterPercentage'](rawSurfaceWater);

        // Then
        expect(waterPercentage).toBe(0.5);
    });

    it('should get zero water percentage when unknown', () => {
        // Given
        const rawSurfaceWater = 'unknown';

        // When
        const waterPercentage: number = transformer['getWaterPercentage'](rawSurfaceWater);

        // Then
        expect(waterPercentage).toBe(0);
    });

    it('should transform planet response', () => {
        // Given
        const response: PlanetResponse = {
            name: 'Tatooine',
            surface_water: '1',
            terrain: 'desert',
            diameter: '10465',
        };
        const expectedPlanet: Planet = {
            name: 'Tatooine',
            waterPercentage: 1,
            terrains: [TerrainType.desert],
            diameter: 10465,
        };

        // When
        const planet: Planet = transformer.transform(response);

        // Then
        expect(planet).toEqual(expectedPlanet);
    });
});
