import { PlanetService } from './planet.service';
import { PlanetResponseTransformer } from './planet-response.transformer';
import Mocked = jest.Mocked;
import { PlanetRepository } from './planet.repository';
import createMockInstance from 'jest-create-mock-instance';
import { Planet, PlanetResponse } from '../../shared/planets/planet.model';

describe('PlanetService', () => {
    let service: PlanetService;

    let planetRepository: Mocked<PlanetRepository>;
    let planetTransformer: Mocked<PlanetResponseTransformer>;

    beforeEach(() => {
        planetRepository = createMockInstance(PlanetRepository);
        planetTransformer = createMockInstance(PlanetResponseTransformer);
        service = new PlanetService(planetRepository, planetTransformer);
    });

    it('should get planets by URI', async () => {
        const uris = ['http://fake-url-1', 'http://fake-url-2'];
        const planetResponse1: PlanetResponse = ({} as unknown) as PlanetResponse;
        const planetResponse2: PlanetResponse = ({} as unknown) as PlanetResponse;
        const planet1 = ({} as unknown) as Planet;
        const planet2 = ({} as unknown) as Planet;
        planetRepository.getByUri.mockResolvedValueOnce(planetResponse1);
        planetRepository.getByUri.mockResolvedValueOnce(planetResponse2);
        planetTransformer.transform.mockReturnValueOnce(planet1);
        planetTransformer.transform.mockReturnValueOnce(planet2);

        // When
        const planets = await service.getPlanetsByUri(uris);

        // Then
        expect(planetRepository.getByUri).toHaveBeenCalledWith(uris[0]);
        expect(planetRepository.getByUri).toHaveBeenCalledWith(uris[1]);
        expect(planetTransformer.transform).toHaveBeenCalledWith(planetResponse1);
        expect(planetTransformer.transform).toHaveBeenCalledWith(planetResponse2);
        expect(planets).toEqual([planet1, planet2]);
    });
});
