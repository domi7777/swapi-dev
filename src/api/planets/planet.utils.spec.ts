import { Planet, TerrainType } from '../../shared/planets/planet.model';
import { PlanetUtils } from '../../shared/planets/planet.utils';

describe('PlanetUtils', () => {
    const basePlanet: Planet = Object.seal({
        name: 'some planet',
        waterPercentage: 1,
        terrains: [],
        diameter: 1000,
    });

    it('should get planets with water and mountains', () => {
        // Given
        const planets: Planet[] = [
            {
                ...basePlanet,
                name: 'Tatooine',
                waterPercentage: 1,
                terrains: [TerrainType.desert],
            },
            {
                ...basePlanet,
                name: 'Mustafar',
                waterPercentage: 0,
                terrains: [TerrainType.mountains],
            },
            {
                ...basePlanet,
                name: 'Alderaan',
                waterPercentage: 40,
                terrains: [TerrainType.grasslands, TerrainType.mountains],
            },
            {
                ...basePlanet,
                name: 'Yavin IV',
                waterPercentage: 8,
                terrains: [TerrainType.jungle, TerrainType.rainforests],
            },
            {
                ...basePlanet,
                name: 'Dagobah',
                waterPercentage: 8,
                terrains: [TerrainType.mountain_ranges],
            },
            {
                ...basePlanet,
                name: 'Mygeeto',
                waterPercentage: 8,
                terrains: [TerrainType.mountain],
            },
        ];

        // When
        const planetsWithWaterAndMountains = PlanetUtils.getPlanetsWithWaterAndMountains(planets);

        // Then
        expect(planetsWithWaterAndMountains.map((planet) => planet.name)).toEqual(['Alderaan', 'Dagobah', 'Mygeeto']);
    });

    it('should get total diameter of planets', () => {
        const planets: Planet[] = [
            { ...basePlanet, diameter: 1 },
            { ...basePlanet, diameter: 10 },
            { ...basePlanet, diameter: 100 },
            { ...basePlanet, diameter: 1000 },
        ];

        // When
        const total = PlanetUtils.getTotalDiameter(planets);

        // Then
        expect(total).toBe(1111);
    });
});
