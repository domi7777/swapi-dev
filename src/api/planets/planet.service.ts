import { Service } from 'typedi';
import { PlanetRepository } from './planet.repository';
import { PlanetResponseTransformer } from './planet-response.transformer';
import { Planet, PlanetResponse } from '../../shared/planets/planet.model';
import { simpleCache } from '../cache/simple-cache.decorator';

@Service()
export class PlanetService {
    constructor(private planetRepository: PlanetRepository, private planetTransformer: PlanetResponseTransformer) {}

    async getPlanetsByUri(planetURIs: string[]): Promise<Planet[]> {
        const planetsResponses = await Promise.all(planetURIs.map((planerUri) => this.getByUri(planerUri)));
        return planetsResponses.map((planetResponse) =>
            this.planetTransformer.transform(<PlanetResponse>planetResponse),
        );
    }

    @simpleCache()
    private getByUri(planerUri: string) {
        return this.planetRepository.getByUri(planerUri);
    }
}
