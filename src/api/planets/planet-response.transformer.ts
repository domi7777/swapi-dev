import { Service } from 'typedi';
import { Transformer } from '../transformer';
import { Planet, PlanetResponse, TerrainType } from '../../shared/planets/planet.model';

@Service()
export class PlanetResponseTransformer implements Transformer<PlanetResponse, Planet> {
    private static TERRAINS_SEPARATOR = ',';

    transform(response: PlanetResponse): Planet {
        return {
            diameter: Number(response.diameter),
            waterPercentage: this.getWaterPercentage(response.surface_water),
            name: response.name,
            terrains: this.getTerrains(response.terrain),
        };
    }

    private getTerrains(terrains: string) {
        return terrains
            .split(PlanetResponseTransformer.TERRAINS_SEPARATOR)
            .map((terrains) => terrains.trim() as TerrainType);
    }

    private getWaterPercentage(surfaceWater: string) {
        const waterPercentage = Number(surfaceWater);
        return !isNaN(waterPercentage) ? waterPercentage : 0;
    }
}
