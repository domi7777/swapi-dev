import { Service } from 'typedi';
import { PlanetResponse } from '../../shared/planets/planet.model';
import { Repository } from '../repository';

@Service()
export class PlanetRepository extends Repository<PlanetResponse> {
    constructor() {
        super();
    }

    async getPlanet(id: number): Promise<PlanetResponse> {
        return await super.get('planets', id);
    }
}
