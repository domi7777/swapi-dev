export abstract class InputSanitizer {
    static sanitizeNumber(filmNumberInput: string | number): number {
        if (
            !this.isEmptyString(filmNumberInput) &&
            filmNumberInput !== null &&
            filmNumberInput !== undefined &&
            !isNaN(Number(filmNumberInput))
        ) {
            return Number(filmNumberInput);
        }
        throw new Error(`"${filmNumberInput}" is not a valid number!`);
    }

    private static isEmptyString(filmNumberInput: string | number) {
        return typeof filmNumberInput === 'string' && filmNumberInput.trim().length === 0;
    }
}
