import { InputSanitizer } from './input-sanitizer';

describe('InputSanitizer', () => {
    it('should not sanitize invalid number', () => {
        expect(() => InputSanitizer.sanitizeNumber('xxx')).toThrow(new Error('"xxx" is not a valid number!'));
    });

    it('should not sanitize null', () => {
        expect(() => InputSanitizer.sanitizeNumber((null as unknown) as string)).toThrow(
            new Error('"null" is not a valid number!'),
        );
    });

    it('should not sanitize undefined', () => {
        expect(() => InputSanitizer.sanitizeNumber((undefined as unknown) as string)).toThrow(
            new Error('"undefined" is not a valid number!'),
        );
    });

    it('should not sanitize empty string', () => {
        expect(() => InputSanitizer.sanitizeNumber((' ' as unknown) as string)).toThrow(
            new Error('" " is not a valid number!'),
        );
    });

    it('should sanitize valid number', () => {
        expect(InputSanitizer.sanitizeNumber('6')).toBe(6);
    });
});
