export class HttpError {
    constructor(public httpStatusCode: number, public httpStatusText: string, public original: Error) {}
}
