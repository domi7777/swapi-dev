import { Service } from 'typedi';
import axios, { AxiosResponse } from 'axios';
import { HttpError } from './http-error';

@Service()
export class HttpClient {
    async get<T>(url: string): Promise<T> {
        try {
            const response: AxiosResponse<T> = await axios.get(url);
            return response.data;
        } catch (error) {
            throw new HttpError(error?.response?.status, error?.response?.statusText, error);
        }
    }
}
