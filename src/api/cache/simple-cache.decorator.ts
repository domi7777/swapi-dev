import { Configuration, CONFIGURATION_KEY } from '../configuration';
import { Container } from 'typedi';

interface CacheEntry<T> {
    value: T;
    creationDate: Date;
}

type SimpleCache = {
    [key: string]: CacheEntry<unknown>;
};
const cache: SimpleCache = {};

export function simpleCache(timeToLive?: number): MethodDecorator {
    return (target: unknown, methodName: string | symbol, descriptor: PropertyDescriptor) => {
        // TODO unit test
        const originalMethod = descriptor.value;
        //wrapping the original method
        descriptor.value = function (...args: unknown[]) {
            if (!timeToLive) {
                const config: Configuration = Container.get(CONFIGURATION_KEY);
                timeToLive = config.cacheTTL;
            }
            const paramsKeys = args?.map((arg) => JSON.stringify(arg) /*might fail if loop*/).join('_');
            const cacheKey = `${String(methodName)}_${paramsKeys}`;
            const cachedEntry = cache[cacheKey];
            if (timeToLive && cachedEntry?.creationDate?.getTime() > Date.now() - timeToLive) {
                // console.log('found cached entry with key', cacheKey)
                return cachedEntry.value;
            } else {
                const result = originalMethod.apply(this, args);
                cache[cacheKey] = {
                    creationDate: new Date(),
                    value: result,
                };
                return result;
            }
        };
    };
}
