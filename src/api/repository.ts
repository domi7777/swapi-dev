import { Container } from 'typedi';
import { Configuration, CONFIGURATION_KEY } from './configuration';
import { HttpClient } from './http/http-client';

export abstract class Repository<T> {
    private httpClient: HttpClient;
    private config: Configuration;

    protected constructor() {
        this.httpClient = Container.get(HttpClient);
        this.config = Container.get(CONFIGURATION_KEY);
    }

    async get(category: string, id: number): Promise<T> {
        return this.getByUri(`${this.config.baseUrl}/${category}/${id}/`);
    }

    async getByUri(url: string): Promise<T> {
        return this.httpClient.get(`${url}?format=${this.config.format}`);
    }
}
