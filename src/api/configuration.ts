import { Container } from 'typedi';

export const CONFIGURATION_KEY = 'CONFIGURATION_KEY';

export interface Configuration {
    baseUrl: string;
    format: 'json' | 'wookiee';
    cacheTTL: number;
}

const configuration: Configuration = {
    baseUrl: 'https://swapi.dev/api',
    format: 'json',
    cacheTTL: 30 * 60 * 1000,
};

Container.set(CONFIGURATION_KEY, configuration);
