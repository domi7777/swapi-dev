import { Service } from 'typedi';

@Service()
export class CommandLineService {
    getArgs(): string[] {
        return process.argv.slice(2);
    }
}
