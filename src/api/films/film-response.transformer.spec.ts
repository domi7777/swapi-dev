import { FilmResponseTransformer } from './film-response.transformer';
import { Film, FilmResponse } from '../../shared/films/film.model';
import { Planet } from '../../shared/planets/planet.model';

describe('FilmResponseTransformer', () => {
    let transformer: FilmResponseTransformer;

    beforeEach(() => {
        transformer = new FilmResponseTransformer();
    });

    it('should transform film response', () => {
        // Given
        const filmNumber = 6;
        const filmResponse: FilmResponse = {
            title: 'Revenge of the Sith',
            episode_id: 3,
            planets: ['http://swapi.dev/api/planets/1/', 'http://swapi.dev/api/planets/2/'],
        };
        const planets: Planet[] = [
            { name: 'Tatooine', diameter: 123, terrains: [], waterPercentage: 1 },
            { name: 'Alderaan', diameter: 456, terrains: [], waterPercentage: 1 },
        ];
        const expectedFilm: Film = {
            filmNumber: filmNumber,
            title: 'Revenge of the Sith',
            episodeId: 3,
            planets: planets,
        };

        // When
        const film = transformer.transform(filmResponse, filmNumber, planets);

        // Then
        expect(film).toEqual(expectedFilm);
    });
});
