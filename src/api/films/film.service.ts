import { Service } from 'typedi';
import { Film } from '../../shared/films/film.model';
import { FilmRepository } from './film.repository';
import { FilmResponseTransformer } from './film-response.transformer';
import { HttpError } from '../http/http-error';
import { PlanetService } from '../planets/planet.service';
import { InputSanitizer } from '../input-sanitizer/input-sanitizer';
import { simpleCache } from '../cache/simple-cache.decorator';

@Service()
export class FilmService {
    constructor(
        private filmRepository: FilmRepository,
        private filmTransformer: FilmResponseTransformer,
        private planetService: PlanetService,
    ) {}

    @simpleCache()
    public async getFilmWithPlanets(filmNumberInput: string | number): Promise<Film> {
        const filmNumber = InputSanitizer.sanitizeNumber(filmNumberInput);
        console.log(`Will look for film #${filmNumber}`);
        let filmResponse;
        try {
            filmResponse = await this.filmRepository.getFilm(filmNumber);
        } catch (error) {
            if (error instanceof HttpError && error.httpStatusCode === 404) {
                throw new Error(`Film #${filmNumber} does not exist`);
            }
            throw error;
        }
        const planets = await this.planetService.getPlanetsByUri(filmResponse.planets);
        return this.filmTransformer.transform(filmResponse, filmNumber, planets);
    }
}
