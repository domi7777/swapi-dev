import { FilmRepository } from './film.repository';
import { HttpClient } from '../http/http-client';
import Mocked = jest.Mocked;
import { Configuration, CONFIGURATION_KEY } from '../configuration';
import { Container } from 'typedi';
import { FilmResponse } from '../../shared/films/film.model';
import createMockInstance from 'jest-create-mock-instance';

describe('FilmRepository', () => {
    let repository: FilmRepository;

    let httpClient: Mocked<HttpClient>;
    let configuration: Configuration;

    beforeEach(() => {
        httpClient = createMockInstance(HttpClient);
        Container.set(HttpClient, httpClient);
        configuration = {
            format: 'json',
            baseUrl: 'https://fake-url/api',
            cacheTTL: 0,
        };
        Container.set(CONFIGURATION_KEY, configuration);
        repository = new FilmRepository();
    });

    it('should get film', async () => {
        // Given
        const filmResponse: FilmResponse = {
            episode_id: 3,
            title: 'A new Hope',
            planets: [],
        };
        httpClient.get.mockResolvedValue(filmResponse);

        // When
        const film = await repository.getFilm(6);

        // Then
        expect(httpClient.get).toHaveBeenCalledWith('https://fake-url/api/films/6/?format=json');
        expect(film).toBe(filmResponse);
    });

    it('should get film by URI', async () => {
        // Given
        const filmResponse: FilmResponse = {
            episode_id: 3,
            title: 'A new Hope',
            planets: [],
        };
        httpClient.get.mockResolvedValue(filmResponse);

        // When
        const film = await repository.getByUri('https://another-fake-url/api/films/6/');

        // Then
        expect(httpClient.get).toHaveBeenCalledWith('https://another-fake-url/api/films/6/?format=json');
        expect(film).toBe(filmResponse);
    });
});
