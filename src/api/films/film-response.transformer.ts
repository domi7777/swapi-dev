import { Service } from 'typedi';
import { Film, FilmResponse } from '../../shared/films/film.model';
import { Planet } from '../../shared/planets/planet.model';
import { Transformer } from '../transformer';

@Service()
export class FilmResponseTransformer implements Transformer<FilmResponse, Film> {
    transform(response: FilmResponse, filmNumber: number, planets: Planet[]): Film {
        return {
            filmNumber: filmNumber,
            episodeId: response.episode_id,
            title: response.title,
            planets: planets,
        };
    }
}
