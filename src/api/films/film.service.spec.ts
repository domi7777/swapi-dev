import { FilmService } from './film.service';
import { FilmRepository } from './film.repository';
import createMockInstance from 'jest-create-mock-instance';
import { FilmResponseTransformer } from './film-response.transformer';
import { HttpError } from '../http/http-error';
import { Film, FilmResponse } from '../../shared/films/film.model';
import { PlanetService } from '../planets/planet.service';
import { Planet } from '../../shared/planets/planet.model';
import Mocked = jest.Mocked;
import { Container } from 'typedi';
import { Configuration, CONFIGURATION_KEY } from '../configuration';

describe('FilmService', () => {
    let service: FilmService;

    let filmRepository: Mocked<FilmRepository>;
    let filmTransformer: Mocked<FilmResponseTransformer>;
    let planetService: Mocked<PlanetService>;

    beforeEach(() => {
        const config: Configuration = {
            ...Container.get(CONFIGURATION_KEY),
            cacheTTL: 0, // disable caching
        };
        Container.set(CONFIGURATION_KEY, config);
        filmRepository = createMockInstance(FilmRepository);
        filmTransformer = createMockInstance(FilmResponseTransformer);
        planetService = createMockInstance(PlanetService);
        service = new FilmService(filmRepository, filmTransformer, planetService);
    });

    it('should get film', async () => {
        // Given
        const filmNumber = 6;
        const filmResponse: FilmResponse = ({ planets: [] } as unknown) as FilmResponse;
        const planets: Planet[] = [];
        const film: Film = ({} as unknown) as Film;
        filmRepository.getFilm.mockResolvedValue(filmResponse);
        planetService.getPlanetsByUri.mockResolvedValue(planets);
        filmTransformer.transform.mockReturnValue(film);

        // When
        const filmResult = await service.getFilmWithPlanets(filmNumber);

        // Then
        expect(filmRepository.getFilm).toHaveBeenCalledWith(filmNumber);
        expect(planetService.getPlanetsByUri).toHaveBeenCalledWith(filmResponse.planets);
        expect(filmTransformer.transform).toHaveBeenCalledWith(filmResponse, filmNumber, planets);
        expect(filmResult).toBe(film);
    });

    it('should not get film when invalid input', async () => {
        // When
        try {
            await service.getFilmWithPlanets('xxx');
            fail('call should fail');
        } catch (error) {
            // Then
            expect(filmRepository.getFilm).not.toHaveBeenCalled();
            expect(error.message).toBe('"xxx" is not a valid number!');
        }
    });

    it('should not get film when does not exist', async () => {
        // Given
        filmRepository.getFilm.mockRejectedValue(new HttpError(404, 'not_found', new Error()));

        // When
        try {
            await service.getFilmWithPlanets(99);
            fail('call should fail');
        } catch (e) {
            expect(e.message).toBe('Film #99 does not exist');
        }
    });

    it('should not get film when http error', async () => {
        // Given
        filmRepository.getFilm.mockRejectedValue(new Error('Woops'));

        // When
        try {
            await service.getFilmWithPlanets(6);
            fail('call should fail');
        } catch (e) {
            expect(e.message).toBe('Woops');
        }
    });
});
