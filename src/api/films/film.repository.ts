import { Service } from 'typedi';
import { FilmResponse } from '../../shared/films/film.model';
import { Repository } from '../repository';

@Service()
export class FilmRepository extends Repository<FilmResponse> {
    constructor() {
        super();
    }

    async getFilm(id: number): Promise<FilmResponse> {
        return await super.get('films', id);
    }
}
