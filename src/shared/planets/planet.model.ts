export interface PlanetResponse {
    name: string;
    diameter: string;
    terrain: string; // the terrain of this planet. Comma-seperated if diverse.eg: "grasslands, mountains",
    surface_water: string; // The percentage of the planet surface that is naturally occuring water or bodies of water.
}

export enum TerrainType {
    airless_asteroid = 'airless_asteroid',
    barren = 'barren',
    canyons = 'canyons',
    caves = 'caves',
    cityscape = 'cityscape',
    desert = 'desert',
    fields = 'fields',
    forests = 'forests',
    fungus_forests = 'fungus_forests',
    gas_giant = 'gas_giant',
    glaciers = 'glaciers',
    grasslands = 'grasslands',
    grassy_hills = 'grassy_hills',
    ice_canyons = 'ice_canyons',
    ice_caves = 'ice_caves',
    jungle = 'jungle',
    jungles = 'jungles',
    lakes = 'lakes',
    lava_rivers = 'lava_rivers',
    mesas = 'mesas',
    mountain = 'mountain',
    mountain_ranges = 'mountain_ranges',
    mountains = 'mountains',
    ocean = 'ocean',
    plains = 'plains',
    rainforests = 'rainforests',
    rivers = 'rivers',
    rock = 'rock',
    rock_arches = 'rock_arches',
    savanna = 'savanna',
    scrublands = 'scrublands',
    seas = 'seas',
    sinkholes = 'sinkholes',
    swamp = 'swamp',
    swamps = 'swamps',
    tundra = 'tundra',
    volcanoes = 'volcanoes',
}

export interface Planet {
    name: string;
    diameter: number;
    terrains: TerrainType[];
    waterPercentage: number;
}
