import { Planet, TerrainType } from './planet.model';

export abstract class PlanetUtils {
    static getPlanetsWithWaterAndMountains(planets: Planet[]): Planet[] {
        return planets.filter((planet) => {
            return (
                planet.waterPercentage > 0 &&
                planet.terrains.some((terrain) =>
                    [TerrainType.mountains, TerrainType.mountain, TerrainType.mountain_ranges].includes(terrain),
                )
            );
        });
    }

    static getTotalDiameter(planets: Planet[]): number {
        return planets.reduce((total: number, planet: Planet) => total + planet.diameter, 0);
    }
}
