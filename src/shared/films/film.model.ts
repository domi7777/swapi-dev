import { Planet } from '../planets/planet.model';

export interface FilmResponse {
    title: string;
    episode_id: number;
    planets: string[];
}

export interface Film {
    filmNumber: number;
    title: string;
    episodeId: number;
    planets: Planet[];
}
