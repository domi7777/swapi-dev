import 'reflect-metadata';
import express from 'express';
import { Container, Service } from 'typedi';
import { FilmService } from './api/films/film.service';
import { Film } from './shared/films/film.model';

@Service()
export class WebApplication {
    private static port = 8080;

    constructor(private filmService: FilmService) {}

    async start(): Promise<void> {
        const app = express();
        app.get('/api/films/:filmNumber', async (req, res) => {
            const filmNumber = req.params.filmNumber;
            const film: Film = await this.filmService.getFilmWithPlanets(filmNumber);
            res.send(film);
        });
        app.listen(WebApplication.port, () => {
            console.log(`⚡️[server]: Express Server is running at http://localhost:${WebApplication.port}`);
        });
        app.use('/', express.static('dist/swapi-web-app'));
    }
}

(async () => {
    await Container.get(WebApplication).start();
})();
