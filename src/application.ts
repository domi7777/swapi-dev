import 'reflect-metadata';
import { Service } from 'typedi';
import { CommandLineService } from './api/command-line.service';
import { FilmService } from './api/films/film.service';
import { Film } from './shared/films/film.model';
import { PlanetUtils } from './shared/planets/planet.utils';

@Service()
export class Application {
    constructor(private commandLineService: CommandLineService, private filmService: FilmService) {}

    async run(): Promise<void> {
        const args = this.commandLineService.getArgs();
        console.log('Starting Swapi with args', args);
        const filmNumberInput = args[0];

        if (filmNumberInput === undefined) {
            throw new Error('Film number is expected as first argument!');
        }
        const film = await this.filmService.getFilmWithPlanets(filmNumberInput);
        await this.calculatePlanetsDiameter(film);
    }

    private async calculatePlanetsDiameter(film: Film): Promise<void> {
        const planetsWithWaterAndMountains = PlanetUtils.getPlanetsWithWaterAndMountains(film.planets);
        const totalDiameter = PlanetUtils.getTotalDiameter(planetsWithWaterAndMountains);

        console.log(
            `\nIn Film #${film.filmNumber} "${film.title}", there are ${planetsWithWaterAndMountains.length} planet(s) ` +
                `that have mountains and a water surface (> 0):`,
        );
        planetsWithWaterAndMountains.forEach((planet) =>
            console.log(
                `- ${planet.name}, ` +
                    `diameter: ${planet.diameter}, ` +
                    `water percentage: ${planet.waterPercentage.toFixed(1)}`,
            ),
        );
        console.log(`Total diameter: ${totalDiameter}`);
    }
}
