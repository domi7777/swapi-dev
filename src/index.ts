import { Container } from 'typedi';
import { Application } from './application';
import { ConsoleColor } from './api/console-color';

(async () => {
    try {
        await Container.get(Application).run();
    } catch (error) {
        console.log(`\n${ConsoleColor.red}`, `And error occured: ${error.message}`);
        console.log(
            ConsoleColor.cyan,
            `
        Usage:
            $ node dist/index.js [film number (1 to 6)]

        eg:
            $ node dist/index.js 6`,
        );
    }
})();
