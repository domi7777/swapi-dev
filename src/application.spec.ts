import { Application } from './application';
import { FilmService } from './api/films/film.service';
import { CommandLineService } from './api/command-line.service';
import createMockInstance from 'jest-create-mock-instance';
import Mocked = jest.Mocked;
import { Film } from './shared/films/film.model';

describe('Application', () => {
    let app: Application;

    let commandLineService: Mocked<CommandLineService>;
    let filmService: Mocked<FilmService>;

    beforeEach(() => {
        commandLineService = createMockInstance(CommandLineService);
        filmService = createMockInstance(FilmService);
        app = new Application(commandLineService, filmService);
    });

    it('should get film', async () => {
        // Given
        const film: Film = {
            filmNumber: 6,
            title: 'Revenge of the Siths',
            episodeId: 3,
            planets: [],
        };
        commandLineService.getArgs.mockReturnValue(['6']);
        filmService.getFilmWithPlanets.mockResolvedValue(film);

        // When
        await app.run();

        // Then
        expect(commandLineService.getArgs).toHaveBeenCalled();
        expect(filmService.getFilmWithPlanets).toHaveBeenCalledWith('6');
    });
});
