import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Film } from '../../shared/films/film.model';
import { map } from 'rxjs/operators';
import { PlanetUtils } from '../../shared/planets/planet.utils';
import { Planet } from '../../shared/planets/planet.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    filmTitles = [
        'A New Hope',
        'The Empire Strikes Back',
        'The Return of The Jedi',
        'The Phantom Menace',
        'Attack of The Clones',
        'Revenge of The Sith',
    ];
    title = 'SWAPI exercise';

    film$: Subject<Film> = new Subject<Film>();
    planets$: Observable<Planet[]> = this.film$.pipe(map((film) => film.planets));
    planetsWithWaterAndMountains$: Observable<Planet[]> = this.planets$.pipe(
        map((planets) => PlanetUtils.getPlanetsWithWaterAndMountains(planets)),
    );
    totalDiameterOfPlanetsWithWaterAndMountains$: Observable<number> = this.planetsWithWaterAndMountains$.pipe(
        map((planets) => PlanetUtils.getTotalDiameter(planets)),
    );
    isLoading = false;
    hasResults = false;
    selectedFilmNumber?: number;

    constructor(private httpClient: HttpClient) {}

    selectFilm(filmNumber: number): void {
        this.isLoading = true;
        this.selectedFilmNumber = filmNumber;
        this.httpClient.get<Film>(`/api/films/${filmNumber}`).subscribe((film) => {
            this.film$.next(film);
            this.isLoading = false;
            this.hasResults = true;
        });
    }

    getImageName(title: string): string {
        return title.replace(/\s/g, '-').toLowerCase();
    }
}
