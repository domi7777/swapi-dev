import { AppComponent } from './app.component';
import { HttpClient } from '@angular/common/http';
import createMockInstance from 'jest-create-mock-instance';
import { BehaviorSubject } from 'rxjs';
import { Film } from '../../shared/films/film.model';
import Mocked = jest.Mocked;

describe('AppComponent', () => {
    let component: AppComponent;

    let httpClient: Mocked<HttpClient>;

    beforeEach(() => {
        httpClient = createMockInstance(HttpClient);
        component = new AppComponent(httpClient);
    });

    it('should select film', () => {
        const filmResponse: Film = {
            planets: [],
            filmNumber: 6,
            title: 'Revenge of Siths',
            episodeId: 3,
        };
        let film: Film | null = null;

        // Given
        httpClient.get.mockReturnValue(new BehaviorSubject(filmResponse));
        component.film$.subscribe((_film) => (film = _film));

        // When
        component.selectFilm(6);

        // Then
        expect(httpClient.get).toHaveBeenCalled();
        expect(film).toBe(filmResponse);
    });
});
