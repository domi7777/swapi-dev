module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    collectCoverageFrom: [
        'src/**/*.{js,ts}',
        '!**/node_modules/**',
        '!**/dist/**',
        '!**/coverage/**',
        '!**/*.module.ts',
        '!**/environment*.ts',
        '!**/polyfills.ts',
        '!**/main.ts',
    ],
    roots: [
        'src'
    ],
};
